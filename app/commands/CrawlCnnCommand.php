<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CrawlCnnCommand extends Command {

	protected $threadTitle;
	/**
	 * Storage the number of crawled comment
	 * 
	 * @var int
	 */
	protected $noOfCrawledComment = 0;

	/**
	 * Nest function limit
	 * 
	 * @var int
	 */
	// protected $limit = 500;

	/**
	 * Memory limit
	 * 
	 * @var int
	 */
	protected $memLimit = -1;

	/**
	 * Count duplitcate times for break out of old post
	 * 
	 * @var int
	 */
	protected $duplicateCounter = 0;

	/**
	 * Number of consecutive duplicates to be consider that we should speed up.
	 *
	 * Some hot post has very high comment up speed that makes the comment being
	 * loaded seem to be duplicated.
	 * 
	 * @var int
	 */ 
	protected $speedLimit = 50;

	/**
     *  Number of consecutive duplicates to be consider that post has old comments
     *
     *  @var int
     */
	protected $duplicateLimit = 100;

	/**
	 * Insert one comment to database
	 *
	 * @param comment-object $element 
	 * @param string $link link to thread
	 *
	 * @return void
	 */
	protected function insertToDatabase($element, $link) {
        $comment = new CnnComment();
        $comment->thread_id = $element->thread;
        $comment->thread_link = $link;
        $comment->thread_title = $this->threadTitle;
        if ($element->author->isAnonymous == false) {
            $comment->author_name = $element->author->name;
            $comment->author_profile_url = $element->author->profileUrl;
            $comment->author_reputation = $element->author->reputation;
        }
        $comment->comment_id = $element->id;
        $comment->comment_parent = $element->parent;
        $comment->comment_likes = $element->likes;
        $comment->comment_dislike = $element->dislikes;
        // $comment->comment_report = $element->numReports;
        $comment->comment_message = $element->raw_message;
        try {
            $comment->save();
            $this->noOfCrawledComment++;
        } catch (Illuminate\Database\QueryException $ex) {
            throw($ex);
        } catch(ErrorException $ex){
        	throw($ex);
        }
    }


    /**
	 * Handle duplicate
	 *
	 * Return true when a number of consecutive duplicates occur
	 *
	 * @return bool
	 */
    protected function duplicateTrigger(){
    	$this->duplicateCounter++;
    	// $this->info($this->duplicateCounter);
    	if($this->duplicateCounter < $this->duplicateLimit)
    	{
    		return false;
    	}else{
    		$this->resetDuplicateCounter();
    		return true;
    	}
    }

    /**
	 * Speedup trigger
	 *
	 * Return true if clawler need to speed up
	 *
	 * @return bool
	 */
    protected function speedTrigger(){
    	if($this->duplicateCounter != $this->speedLimit)
    	{
    		return false;
    	}else{
    		return true;
    	}
    }

    /**
	 * Reset duplicate counter
	 * 
	 * @return void
	 */
    protected function resetDuplicateCounter(){
    	$this->duplicateCounter = 0;
    }

    /**
	 * Get title of the post
	 *
	 * @param str $link
	 *
	 * @return void
	 */
	protected function getTitle($link){
		try{
			$content = file_get_html($link);
			$title = @$content->find("head title")[0]->innertext;
			$this->info("Title: ".$title);
			$this->threadTitle = $title;
		}catch(ErrorException $ex){
			throw $ex;
		}
	}

	/**
	 * Get comment of one post
	 *
	 * @param string $url url to the post
	 *
	 * @return void
	 */
	protected function getComments($url)
	{
		//get post info
		$threadLink = $url;
		$url = strstr($url,"cnn.com");
		$url = str_replace("cnn.com","http://disqus.com/embed/comments/?base=default&disqus_version=634c8a73&f=cnn&t_i=",$url);
		$url = strstr($url, "index.html", true);
		$url = $url."index.html";
		// $this->info($url);
		$this->info("Thread link: ".$threadLink);
		try{
			// $this->info("Trying to get post info");
			$this->getTitle($threadLink);
			$html = file_get_html($url);
		}catch(ErrorException $ex){
			Log::error("Connection error ".$ex);
			Log::info("Link: ".$url);
			return;
		}
		try{
			$threadIdPart = @$html->find("script[id=disqus-threadData]")[0]->innertext;
			// $this->info("$threadIdPart");
			$json = json_decode($threadIdPart);
			$threadId = $json->response->thread->id;
		}catch(ErrorException $ex){
			if(strstr($ex->getMessage(),"Trying to get property of non-object") != false)
			{
				$this->info("This post has 0 comment");
				return;
			}
			if(strstr($ex->getMessage(),"Undefined offset: 0") != false)
			{
				Log::error("Something wierd: ". $ex);
				return;
			}
		}
		$totalComment = $json->cursor->total;
		$noOfPart = ($totalComment - ($totalComment % 50)) / 50 + 1;
		
		$this->info("ThreadID: ".$threadId);
		$this->info("Total comment: ".$totalComment);
		$this->info("Number of Part: ".$noOfPart);
		
		//use the info to get comments
		$cursor = 0;
		for($cursor; $cursor < $noOfPart; $cursor++)
		{
			$currentPart = $cursor + 1;
			$this->info("Processing part ".$currentPart." of ".$noOfPart);
			$link = "http://disqus.com/api/3.0/threads/listPostsThreaded?limit=50&thread=".$threadId."&forum=cnn&order=desc&cursor=".$cursor."%3A0%3A0&api_key=E8Uh5l5fHZ6gD8U3KycjAIAk46f68Zw7C6eW8WSjZvCLXebZ7p0r1yrYDrLilk2F";
			// $this->info("link: ".$link);
			try{
				$html = file_get_html($link);
			}catch(ErrorException $ex){
				Log::error("Connection error ".$ex);
				Log::info("Link: ".$link);
				continue;
			}
			// $this->info("encoded: ".$html);
			$json = json_decode($html);
			try {	
				foreach ($json->response as $cnnComment) {
					if(strlen($cnnComment->raw_message) > 30)
					{
	            		try {
	            			$this->insertToDatabase($cnnComment,$threadLink);
	            			$this->resetDuplicateCounter();	
			            } catch (Illuminate\Database\QueryException $ex) {
			            	// Log::error($ex);
			            	if(strstr($ex->getMessage(), "Duplicate") != false)
			            	{
			            		if($this->duplicateTrigger()){
			            			$this->info("Duplicate -> Break");
			            			$cursor = $noOfPart;
			            			break;
			            		}
			            		if($this->speedTrigger()){
			            			$this->info("Speedup!");
			            			$cursor++;
			            			$noOfPart++;
			            		}
			            	}else{
			            		$this->info("Something went wrong with the database, please check the log file");
			            		Log::error($ex);
			            	}
		            	}
	            	}
	            }
            } catch(ErrorException $ex){
            	if(strstr($ex->getMessage(), "Trying to get property of non-object") != false)
            	{
            		Log::info("Fail to get part ".$cursor." of ".$noOfPart);
            		Log::info($threadLink);
            	}else{
	            	$this->info("Something went wrong, please check the log file");
	            	Log::error($ex);
	            }
			}
		}
	}


	/**
	 * Crawl the posts on one category page
	 *
	 * @return void
	 */
	protected function crawl()
	{
		$categoryList = array();
        $handle = fopen(app_path() . '/CnnCategory.txt', 'r');
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $categoryList[] = $line;
            }
        }
        fclose($handle);
        foreach ($categoryList as $category) {
			// $this->info($category);
			$link = trim($category);

			$this->info("Getting post from: ".$link);
			try{
				$content = file_get_html($link);
				$anchors = @$content->find("div[class=archive-item story cnn_skn_spccovstrylst] h2 a");
				foreach ($anchors as $anchor) {
					$linkToPost = "http://edition.cnn.com".$anchor->href;
					$this->getComments($linkToPost);
				}
				
			}catch(ErrorException $ex){
				Log::error("Connection error ".$ex);
				Log::info("Link: ".$link);
			}
		}
		$this->info($this->noOfCrawledComment." comments saved.");
		$this->info("Done, check the log file");
	}

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:crawlcnn';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Crawl CNN comments';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// $this->getComments($link);
		ini_set('memory_limit', $this->memLimit);
		// ini_set('xdebug.max_nesting_level', $this->limit);
		$this->crawl();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::OPTIONAL, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
