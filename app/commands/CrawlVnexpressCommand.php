<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CrawlVnexpressCommand extends Command {

    protected $pageNum;
    protected $spamFilter;

    protected function insertIntoDatabase($element, $categoryId, $link) {
        if (strlen($element->content) >= $this->spamFilter) {
            $comment = new CrawledComment;
            $comment->category_id = $categoryId;
//        $this->info("getting artical_id");
            $comment->article_id = $element->article_id;
//        $this->info("getting comment_id");
            $comment->comment_id = $element->comment_id;
//        $this->info("getting title");
            $comment->title = $element->title;
//        $this->info("getting content");
            // var_dump($element->content)
            $comment->content = html_entity_decode(strip_tags($element->content));
//        $this->info("getting fullname");
            $comment->full_name = $element->full_name;
//        $this->info("getting userlike");
            $comment->userlike = $element->userlike;
//        $this->info("getting parent_id");
            $comment->parent_id = $element->parent_id;
            $comment->thread_url = $link;
            try {
                $comment->save();
            } catch (Illuminate\Database\QueryException $ex) {
//            $this->info("Duplicated");
                throw($ex);
            };
        }
    }

    public function updateComment($URL) {

        $link = $URL;
        if (!is_null($link)) {
            $content = file_get_html($link);
            $this->info("Processing: " . $link);
            $elems = $content->find("//div/div/div/script[2]");
            if ($elems == NULL)
                return;
            $this->info("found page info");
            $part = strstr($elems[0], "objectid:");
            if ($part == false)
                return;
            $objectIdPart = str_split($part, 9)[1];
            $objectId = intval($objectIdPart);
            $part = strstr($elems[0], "objecttype:");
            //$this->info("abc");
            $objectTypePart = str_split($part, 11)[1];
            $objectType = intval($objectTypePart);
            //
            $part = strstr($elems[0], "categoryid:");
            $categoryIdPart = str_split($part, 11)[1];
            $categoryId = intval($categoryIdPart);
            //
            $jsonURL = "http://interactions.vnexpress.net/index/get?offset=0&limit=24&frommobile=0&sort=time&objectid=" . $objectId . "&objecttype=" . $objectType . "&siteid=1000000";
            try {
                $json = file_get_contents($jsonURL);
            } catch (ErrorException $ex) {
                Log::error('Connection Error: ' . $ex);
            }
            if (strcmp($json, 'defaultCallback({"error":0,"errorDescription":"Invalid article id","data":[]});') == 0)
                return;
            $json = substr($json, 16);
            $json = rtrim($json, ";");
            $json = rtrim($json, ")");
            $json = json_decode($json);
            $total = $json->data->total;
            $this->info("found " . $json->data->totalitem . " comments");
            for ($i = 0; $i < $total; $i += 50) {
                $jsonURL = "http://interactions.vnexpress.net/index/get?offset=" . $i . "&limit=50&sort=time&objectid=" . $objectId . "&objecttype=" . $objectType . "&siteid=1000000";
                try {
                    $json = file_get_contents($jsonURL);
                } catch (ErrorException $ex) {
                    Log::error('Connection Error: ' . $ex);
                }
                $json = substr($json, 16);
                $json = rtrim($json, ";");
                $json = rtrim($json, ")");
                $json = json_decode($json);

                foreach ($json->data->items as $element) {
                    try {
                        $this->insertIntoDatabase($element, $categoryId, $link);
                    } catch (Illuminate\Database\QueryException $ex) {
                        $this->info("Duplicated");
                        Log::info($ex);
                        break;
                    }
                    if ($element->replys->total != 0) {
//                    $this->insertIntoDatabase($element->replys->items[0]);
                        try {
                            $this->insertIntoDatabase($element->replys->items[0], $categoryId, $link);
                        } catch (Illuminate\Database\QueryException $ex) {
                            $this->info("Duplicated");
                            Log::info($ex);
                            break;
                        }
                        if ($element->replys->total > 1) {
//                        $this->insertIntoDatabase($element->replys->items[1]);
                            try {
                                $this->insertIntoDatabase($element->replys->items[1], $categoryId, $link);
                            } catch (Illuminate\Database\QueryException $ex) {
                                $this->info("Duplicated");
                                Log::info($ex);
                                break;
                            }
                        }
                        if ($element->replys->total > 2) {
                            $replyURL = "http://interactions.vnexpress.net//index/getreplay?siteid=1002592&objectid=" . $objectId . "&objecttype=" . $objectType . "&id=" . $element->comment_id . "&limit=100&offset=2";
                            try {
                                $replyJson = file_get_contents($replyURL);
                            } catch (ErrorException $ex) {
                                Log::error('Connection Error: ' . $ex);
                            }
                            $replyJson = substr($replyJson, 16);
                            $replyJson = rtrim($replyJson, ";");
                            $replyJson = rtrim($replyJson, ")");
                            $replyJson = json_decode($replyJson);
                            try {
                                foreach ($replyJson->data->items as $element1) {
                                    try {
                                        $this->insertIntoDatabase($element1, $categoryId, $link);
                                    } catch (Illuminate\Database\QueryException $ex) {
                                        $this->info("Duplicated");
                                        Log::info($ex);
                                        break;
                                    }
                                }
                            } catch (ErrorException $ex) {
                                $this->error('Connection Error:' . $ex);
                            };
                        }
                    }
                }
            }
        }
    }

    public function Crawlvne() {
        $html = file_get_html("http://vnexpress.net/");
        $ul = $html->find('ul[id=menu_web]')[0];
        $a = $ul->find('a');
        $hrefs = [];
        foreach ($a as $category) {
            if ($category->href[0] == '/')
                $category->href = 'http://vnexpress.net' . $category->href;
            if ($category->href[0] == "h") {
                $hrefs[] = $category->href;
                for ($i = 2; $i <= $this->pageNum; $i++) {
                    $hrefs[] = $category->href . '/page/' . $i . '.html';
                }
            }
        }
        foreach ($hrefs as $h) {
            if ($h != "http://vnexpress.net" && (((strstr($h, "http://raovat.vnexpress.net") == false) && (strstr($h, "http://video.vnexpress.net") == false)))) {
                $this->info($h);
                $html1 = file_get_html($h);
                $linkcrwl = $html1->find("ul[id=news_home] a[class=icon_commend]");
                foreach ($linkcrwl as $l)
                    $this->updateComment($l->href);
            }
        }


        $this->info("Finished!");
        $this->info("Please check the log file");
    }

    public function CrawlCategory($URL) {
        if ($URL == "http://vnexpress.net")
            $this->Crawlvne();
        else {
            $hrefs = [];
            $hrefs[] = $URL;
            for ($i = 2; $i <= $this->pageNum; $i++) {
                $hrefs[] = $URL . '/page/' . $i . '.html';
            }
            foreach ($hrefs as $h) {
                $this->info($h);
                $html1 = file_get_html($h);
                $linkcrwl = $html1->find("ul[id=news_home] a[class=icon_commend]");
                //$this->info('abc');
                foreach ($linkcrwl as $l) {
                    $this->updateComment($l->href);
                }
            }
            $this->info("Finished!");
            $this->info("Please check the log file");
        }
    }

    public function Welcome() {
        $URL = '';
        $category = $this->ask(" Nhap 1 de chon muc Thoi su.\n Nhap 2 de chon muc Goc nhin.\n Nhap 3 de chon muc The gioi.
            \n Nhap 4 de chon muc Kinh doanh.\n Nhap 5 de chon muc Giai tri.\n Nhap 6 de chon muc The thao.
            \n Nhap 7 de chon muc Phap luat.\n Nhap 8 de chon muc Giao duc.\n Nhap 9 de chon muc Doi song.
            \n Nhap 10 de chon muc Du lich.\n Nhap 11 de chon muc Khoa hoc.\n Nhap 12 de chon muc So hoa.\n Nhap 13 de chon muc Xe.
            \n Nhap 14 de chon muc Cong dong.\n Nhap 15 de chon muc Tam su.\n Nhap 16 de chon muc Cuoi\n", 0);
        switch ($category) {
            case 1:
                $URL = 'http://vnexpress.net/tin-tuc/thoi-su';
                break;
            case 2:
                $URL = "http://vnexpress.net/tin-tuc/goc-nhin";
                break;
            case 3:
                $URL = "http://vnexpress.net/tin-tuc/the-gioi";
                break;
            case 4:
                $URL = "http://kinhdoanh.vnexpress.net/";
                break;
            case 5:
                $URL = "http://giaitri.vnexpress.net/";
                break;
            case 6:
                $URL = "http://thethao.vnexpress.net/";
                break;
            case 7:
                $URL = "http://vnexpress.net/tin-tuc/phap-luat/";
                break;
            case 8:
                $URL = "http://vnexpress.net/tin-tuc/giao-duc";
                break;
            case 9:
                $URL = "http://doisong.vnexpress.net/";
                break;
            case 10:
                $URL = "http://dulich.vnexpress.net/";
                break;
            case 11:
                $URL = "http://vnexpress.net/tin-tuc/khoa-hoc";
                break;
            case 12:
                $URL = "http://sohoa.vnexpress.net/";
                break;
            case 13:
                $URL = "http://vnexpress.net/tin-tuc/oto-xe-may/";
                break;
            case 14:
                $URL = "http://vnexpress.net/tin-tuc/cong-dong";
                break;
            case 15:
                $URL = "http://vnexpress.net/tin-tuc/tam-su";
                break;
            case 16:
                $URL = "http://vnexpress.net/tin-tuc/cuoi";
                break;
            default:
                $URL = "http://vnexpress.net";
                break;
        }
        $this->CrawlCategory($URL);
    }

    protected $name = 'command:crawlvne';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clawl vnexpress comments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.Welcome
     *
     * @return mixed
     */
    public function fire() {
        $this->pageNum = $this->option('pageNum');
        $this->spamFilter = $this->option('spamFilter');
        $this->Welcome();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array(
            array('example', InputArgument::OPTIONAL, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array(
            array('pageNum', "p", InputOption::VALUE_OPTIONAL, 'Number of page to crawl each category', 2),
            array('spamFilter', "s", InputOption::VALUE_OPTIONAL, 'Minimum length of comment', 20),
        );
    }

}
