<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class GetPendingDisqusCommentCommand extends Command {

	protected $url;
    protected $disqus_identifier;
    protected $disqus_shortname;
    protected $threadId;
    protected $threadTitle;
    /**
	 * Storage the number of crawled comment
	 * 
	 * @var int
	 */
	protected $noOfCrawledComment = 0;

	/**
	 * Nest function limit
	 * 
	 * @var int
	 */
	// protected $limit = 500;

	/**
	 * Memory limit
	 * 
	 * @var int
	 */
	protected $memLimit = -1;

	/**
	 * Count duplitcate times for break out of old post
	 * 
	 * @var int
	 */
	protected $duplicateCounter = 0;

	/**
	 * Number of consecutive duplicates to be consider that we should speed up.
	 *
	 * Some hot post has very high comment up speed that makes the comment being
	 * loaded seem to be duplicated.
	 * 
	 * @var int
	 */ 
	protected $speedLimit = 50;

	/**
     *  Number of consecutive duplicates to be consider that post has old comments
     *
     *  @var int
     */
	protected $duplicateLimit = 100;

	/**
	 * Get disqus_identifier and disqus_shortname of the post
	 *
	 * @return void
	 */
	protected function getPostInfo(){
		$link = $this->url;
		try{
			$content = file_get_html($link);
		}catch(ErrorException $ex){
			throw $ex;
		}
		$title = $content->find("head title")[0]->innertext;
		$this->info("Title: ".$title);
		$this->threadTitle = $title;
		$text = $content->save();
		$idPart = strstr($text,"disqus_identifier");;
		$idPart = substr($idPart, 17);
		$idPart = trim($idPart);
		$idPart = substr($idPart,1);
		$idPart = trim($idPart);
		$idPart1 = strstr($idPart,",",true);
		$idPart = trim($idPart);
		if(strlen($idPart1) == 0)
		{
			$idPart1 = $idPart;
		}
		$idPart2 = strstr($idPart1,";",true);
		$idPart = trim($idPart);
		if(strlen($idPart2) == 0)
		{
			$idPart2 = $idPart1;
		}
		$idPart = substr($idPart2,0,-1);
		$idPart = substr($idPart,1);
		// $this->info($idPart);

		$namePart = strstr($text,"disqus_shortname");
		$namePart = substr($namePart, 16);
		$namePart = trim($namePart);
		$namePart = substr($namePart,1);
		$namePart = trim($namePart);
		$namePart1 = strstr($namePart,",",true);
		$namePart1 = trim($namePart1);
		if(strlen($namePart1) == 0)
		{
			$namePart1 = $namePart;
		}
		// $this->info($namePart1);
		$namePart2 = strstr($namePart1,";",true);
		$namePart = trim($namePart);
		// $this->info($namePart2);
		if(strlen($namePart2) == 0)
		{
			$namePart2 = $namePart1;
		}
		$namePart = substr($namePart2,0,-1);
		$namePart = substr($namePart,1);
		$this->info($namePart);
		$this->info($idPart);
		$this->disqus_identifier = urlencode($idPart);
		$this->disqus_shortname = urlencode($namePart);
	}

	/**
	 * Insert one comment to database
	 *
	 * @param comment-object $element 
	 * @param string $link link to thread
	 *
	 * @return void
	 */
	protected function insertToDatabase($element, $link) {
        $comment = new CnnComment();
        $comment->thread_id = $element->thread;
        $comment->thread_link = $link;
        $comment->thread_title = $this->threadTitle;
        if ($element->author->isAnonymous == false) {
            $comment->author_name = $element->author->name;
            $comment->author_profile_url = $element->author->profileUrl;
            $comment->author_reputation = $element->author->reputation;
        }
        $comment->comment_id = $element->id;
        $comment->comment_parent = $element->parent;
        $comment->comment_likes = $element->likes;
        $comment->comment_dislike = $element->dislikes;
        // $comment->comment_report = $element->numReports;
        $comment->comment_message = $element->raw_message;
        try {
            $comment->save();
            $this->noOfCrawledComment++;
        } catch (Illuminate\Database\QueryException $ex) {
            throw($ex);
        } catch(ErrorException $ex){
        	throw($ex);
        }
    }

     /**
	 * Handle duplicate
	 *
	 * Return true when a number of consecutive duplicates occur
	 *
	 * @return bool
	 */
    protected function duplicateTrigger(){
    	$this->duplicateCounter++;
    	// $this->info($this->duplicateCounter);
    	if($this->duplicateCounter < $this->duplicateLimit)
    	{
    		return false;
    	}else{
    		$this->resetDuplicateCounter();
    		return true;
    	}
    }

    /**
	 * Speedup trigger
	 *
	 * Return true if clawler need to speed up
	 *
	 * @return bool
	 */
    protected function speedTrigger(){
    	if($this->duplicateCounter != $this->speedLimit)
    	{
    		return false;
    	}else{
    		return true;
    	}
    }

    /**
	 * Reset duplicate counter
	 * 
	 * @return void
	 */
    protected function resetDuplicateCounter(){
    	$this->duplicateCounter = 0;
    }
	/**
	 * Get comment of one post
	 *
	 * @param string $url url to the post
	 *
	 * @return void
	 */
	protected function getComments()
	{
		//get post info
		$threadLink = $this->url;
		$url = "http://disqus.com/embed/comments/?base=default&disqus_version=634c8a73&f=".$this->disqus_shortname."&t_i=".$this->disqus_identifier;
		$this->info("Thread link: ".$threadLink);
		try{
			// $this->info("Trying to get post info");
			$html = file_get_html($url);
		}catch(ErrorException $ex){
			Log::error("Connection error ".$ex);
			Log::info("Link: ".$url);
			return;
		}
		try{
			$threadIdPart = $html->find("script[id=disqus-threadData]")[0]->innertext;
			// $this->info("$threadIdPart");
			$json = json_decode($threadIdPart);
			$threadId = $json->response->thread->id;
			$this->threadId = $threadId;
		}catch(ErrorException $ex){
			if(strstr($ex->getMessage(),"Trying to get property of non-object") != false)
			{
				$this->info("This post has 0 comment");
				return;
			}
			if(strstr($ex->getMessage(),"Undefined offset: 0") != false)
			{
				Log::error("Something wierd: ". $ex);
				return;
			}
		}
		$totalComment = $json->cursor->total;
		$noOfPart = ($totalComment - ($totalComment % 50)) / 50 + 1;
		
		$this->info("ThreadID: ".$threadId);
		$this->info("Total comment: ".$totalComment);
		$this->info("Number of Part: ".$noOfPart);
		
		//use the info to get comments
		$cursor = 0;
		for($cursor; $cursor < $noOfPart; $cursor++)
		{
			$currentPart = $cursor + 1;
			$this->info("Processing part ".$currentPart." of ".$noOfPart);
			$link = "http://disqus.com/api/3.0/threads/listPostsThreaded?limit=50&thread=".$threadId."&order=desc&cursor=".$cursor."%3A0%3A0&api_key=E8Uh5l5fHZ6gD8U3KycjAIAk46f68Zw7C6eW8WSjZvCLXebZ7p0r1yrYDrLilk2F";
			// $this->info("link: ".$link);
			try{
				$html = file_get_html($link);
			}catch(ErrorException $ex){
				Log::error("Connection error ".$ex);
				Log::info("Link: ".$link);
				continue;
			}
			// $this->info("encoded: ".$html);
			$json = json_decode($html);
			try {	
				foreach ($json->response as $cnnComment) {
					if(strlen($cnnComment->raw_message) > 30)
					{
	            		try {
	            			$this->insertToDatabase($cnnComment,$threadLink);
	            			$this->resetDuplicateCounter();	
			            } catch (Illuminate\Database\QueryException $ex) {
			            	// Log::error($ex);
			            	if(strstr($ex->getMessage(), "Duplicate") != false)
			            	{
			            		if($this->duplicateTrigger()){
			            			$this->info("Duplicate -> Break");
			            			$cursor = $noOfPart;
			            			break;
			            		}
			            		if($this->speedTrigger()){
			            			$this->info("Speedup!");
			            			$cursor++;
			            			$noOfPart++;
			            		}
			            	}else{
			            		$this->info("Something went wrong with the database, please check the log file");
			            		Log::error($ex);
			            	}
		            	}
	            	}
	            }
            } catch(ErrorException $ex){
            	if(strstr($ex->getMessage(), "Trying to get property of non-object") != false)
            	{
            		Log::info("Fail to get part ".$cursor." of ".$noOfPart);
            		Log::info($threadLink);
            	}else{
	            	$this->info("Something went wrong, please check the log file");
	            	Log::error($ex);
	            }
			}
		}
		$this->info($this->noOfCrawledComment." comments saved.");
	}

	/**
	 * Update pending status
	 *
	 * @return void
	 */
	protected function updateStatus($url){
		$this->info("update status");
		$pendingLink = PendingDisqusComment::where("thread_link", $url)->get()[0];
		$pendingLink->status = true;
		if($this->noOfCrawledComment > 0) {
		$pendingLink->thread_id = $this->threadId;
		$pendingLink->thread_title = $this->threadTitle;	
		}
		try{
			$pendingLink->save();
		} catch (Illuminate\Database\QueryException $ex) {
            Log::info($ex);
        } catch(ErrorException $ex){
        	Log::info($ex);
        }
	}

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:getPendingDisqus';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Get comments from pending list';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$pendingDisqusLinks = PendingDisqusComment::where("status",false)->get();
		foreach ($pendingDisqusLinks as $link) {
			$this->noOfCrawledComment = 0;
			$this->info($link->thread_link);
			$this->url = $link->thread_link;
			try{
				$this->getPostInfo();
				$this->getComments();
				$this->updateStatus($this->url);
			}catch(ErrorException $ex){
				Log::info($ex);
				$this->info("Connection error, try again later");
				continue;
			}
			
		}
		$this->info("Done, check the log file");
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::OPTIONAL, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
