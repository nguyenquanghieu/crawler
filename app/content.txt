<table align="center" border="0" cellpadding="3" cellspacing="0" class="tplCaption" ><tbody><tr><td>
				<img data-natural- src="http://m.f25.img.vnecdn.net/2014/08/07/Gia-xang-giam-6591-1407405483.jpg" alt="Gia-xang-giam-6591-1407405483.jpg"></td>
		</tr><tr><td>
				<p class="Image">
					Nhân viên cây xăng tại quận Cầu Giấy (Hà Nội) đang thay biểu giá mới, áp dụng từ 16h ngày 7/8. Ảnh: <em>Quý Đoàn</em></p>
			</td>
		</tr></tbody></table><p class="Normal">
	Theo yêu cầu của Liên bộ Tài chính - Công Thương, Tập đoàn Xăng dầu Việt Nam công bố giảm giá bán lẻ xăng dầu từ 70 đồng đến 500 đồng mỗi lít, kg từ 16h hôm nay. Trong đó, mặt hàng xăng RON 92 giảm mạnh nhất, 500 đồng mỗi lít. Các loại dầu giảm từ 70 đồng đến 160 đồng mỗi lít, kg.</p>
<p class="Normal">
	Bảng giá mới của Petrolimex sau khi giảm giá:</p>
<table align="center" border="0" cellpadding="3" cellspacing="0" class="tplCaption" ><tbody><tr><td>
				<img alt="xang-dau-2-3520-1407403851.jpg" data-natural- src="http://m.f25.img.vnecdn.net/2014/08/07/xang-dau-2-3520-1407403851.jpg"></td>
		</tr><tr><td>
				<p class="Image">
					Mặt hàng giá xăng dầu của Petrolimex sau khi giảm. Ảnh: Nguồn Petrolimex</p>
			</td>
		</tr></tbody></table><p class="Normal">
	Theo tính toán của Bộ Tài chính, giá bán hiện hành (chưa tính mức sử dụng Quỹ Bình ổn giá) cao hơn giá cơ sở khoảng 191 đồng mỗi lít. Vì vậy, nếu ngừng sử dụng  quỹ (600 đồng mỗi lít), giá xăng trong nước chỉ có thể giảm tương ứng mức chênh lệch là 191 đồng. Tuy nhiên, để hài hòa lợi ích với người tiêu dùng, Liên Bộ đã yêu cầu doanh nghiệp giảm sử dụng Quỹ xuống còn 300 đồng mỗi lít; đồng thời giảm giá xăng trong nước phù hợp với quy định.</p>
<p class="Normal">
	Nếu tham tham chiếu theo giá hiện hành của Tập đoàn xăng dầu Việt Nam, thì mức giảm tối thiểu đối với xăng RON 92 khoảng 491 đồng mỗi lít.</p>
<p class="Normal">
	Với dầu điêzen, dầu hỏa và dầu madút, Liên bộ yêu cầu giảm giá bán tối thiểu tương ứng với mức chênh lệch giữa giá bán hiện hành cao hơn giá cơ sở. <span>Giá bán sau khi điều chỉnh giảm không cao hơn giá cơ sở theo quy định, trong đó</span><span> giá cơ sở của dầu điezel 0,05S là 22.177 đồng mỗi lít; dầu hỏa là 22.322 đồng mỗi lít, </span><span style="color:rgb(34,34,34);">dầu madút 3,5S là 18.442 đồng mỗ kg.</span></p>
<p class="Normal">
	<span>Từ đầu năm đến nay, giá xăng dầu đã trải qua 18 lần điều chỉnh, trong đó có 5 lần tăng, 8 lần giảm. Các lần còn lại là điều chỉnh mức sử dụng quỹ bình ổn và trích lợi nhuận định mức để giữ giá. Trong đó, mặt hàng xăng được điều chỉnh tăng 5 lần và 2 lần giảm. Lần tăng mạnh nhất vào ngày 7/7 từ 130 đồng đến 420 đồng đưa giá xăng bán lẻ lập kỷ lục lên tới 25.640 đồng mỗi lít. </span></p>
<p class="Normal">
	Theo tính toán của Bộ Tài chính, một lít xăng RON 92 về Việt Nam phải chịu thuế nhập khẩu (18%), thuế tiêu thụ đặc biệt (10%), thuế bảo vệ môi trường (1.000 đồng), thuế giá trị gia tăng (10%), tương đương 8.244 đồng.</p>
<p class="Normal" style="text-align:right;">
	<strong>Hoàng Lan</strong></p>                                        