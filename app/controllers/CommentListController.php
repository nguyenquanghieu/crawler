<?php

class CommentListController extends BaseController {

    public $layout = "layouts.admin";
    private $anchor = [];
    
    public function getIndex(){
        $this->layout->nest("content","index");
    }
    
    public function getBack(){
        return Redirect::back();
    }
    
    public function getSearchExpress(){
        $keyword = Input::get('keyword');
        $op = Input::get("op");
        $query = ($op == 1) ? CrawledComment::whereRaw("title LIKE ?",["%$keyword%"])->orWhere("content","LIKE","%".$keyword."%") : CrawledComment::whereRaw("title LIKE ?",["%$keyword%"]);
        $get= $query->get();
        $result = ["article_id"=>[],"title"=>[],"updated_ad"=>[],"total"=>[]];
        $exist=[];
        foreach($get as $g){
            if (!in_array($g->article_id, $exist)){
                $exist[]=$g->article_id;
                $result["article_id"][] = $g->article_id;
                $title = explode("-", $g->title)[0];
                $result["title"][] = $title;
                $result["updated_at"][] = $g->updated_at;
                $result["total"][] = count(CrawledComment::where("article_id","=",$g->article_id)->get());
            }
        }
        $this->layout->nest("content","express",['result'=>$result]);
    }
    public function getSearchCnn(){
        $keyword = Input::get('keyword');
        $op = Input::get("op");
        $query = ($op==1) ? CnnComment::whereRaw("thread_title LIKE ?",["%$keyword%"])->orWhere("comment_message","LIKE","%".$keyword."%") : CnnComment::whereRaw("thread_title LIKE ?",["%$keyword%"]);
        $get= $query->get();
        $result = ["thread_id"=>[],"updated_ad"=>[],"total"=>[]];
        $exist=[];
        foreach($get as $g){
            if (!in_array($g->thread_id, $exist)){
                $exist[]=$g->thread_id;
                $result["thread_id"][] = $g->thread_id;
                $result["title"][] = $g->thread_title;
                $result["updated_at"][] = $g->updated_at;
                $result["total"][] = count(CnnComment::where("thread_id","=",$g->thread_id)->get());
            }
        }
        $this->layout->nest("content","cnn",['result'=>$result]);
    }

    public function getExpress() {
        $this->layout->nest("content", "express");
    }
    
    public function getCnn() {
        $this->layout->nest("content", "cnn");
    }
    
    public function getArticle($id){
        $cmts = CrawledComment::where('article_id','=',$id)->paginate(15);
        $this->layout->nest("content","result", ["cmts"=>$cmts,"a_id"=>$id]);
    }
    
    public function getThread($id){
        $cmts = CnnComment::where('thread_id','=',$id)->paginate(15);
        $this->layout->nest("content","result_cnn", ["cmts"=>$cmts,"t_id"=>$id]);
    }
    
    public function  getFile($a_id){
        $this->layout->nest("content","uploadfile",array('a_id'=>$a_id));
    }
    
    public function  getFileCnn($t_id){
        $this->layout->nest("content","uploadfilecnn",array('t_id'=>$t_id));
    }
    
    public function postFile(){
        $a_id = Input::get('a_id');
        $destinationPath = 'uploads/';
        $filename = Input::file('upfile')->getClientOriginalName();
        $extension =Input::file('upfile')->getClientOriginalExtension();
        if ($extension != "xls" && $extension != "xlsx") {
            Log::info("You must upload an Excel file");
        } else {
            $uploadSuccess = Input::file('upfile')->move($destinationPath, $filename);
            if ($uploadSuccess) {
                $objReader = new PHPExcel_Reader_Excel2007();
                $objReader->setReadDataOnly(true);
                $objPHPExcel = $objReader->load($destinationPath.$filename);
                $sheetData = $objPHPExcel->getSheet(0)->toArray();
                $anc = ["keyword"=>[],"landpage"=>[]];
                foreach($sheetData as $s){
                    $this->anchor[] = "<a href='".$s[1]."'>".$s[0]."</a>";
                    $anc["keyword"][] = $s[0];
                    $anc["landpage"][] = $s[1];
                }
                
                $cmts = CrawledComment::where('article_id','=',$a_id)->get();
                $title = explode("-",$cmts[0]->title)[0];
                $a_txt = "{";
                $pattern = "/(\n|\t|\/|\\|\"|\'|\?|%|@|\*|#|$|&)*/";
                $objPHPExcel1 = new PHPExcel();
                $objPHPExcel1->getProperties()->setCreator("ExportResult")
                                            ->setLastModifiedBy("Khanh Truong")
                                            ->setTitle("Office 2007 XLSX Test Document")
                                            ->setSubject("Office 2007 XLSX Test Document")
                                            ->setDescription("Test doc for Office 2007 XLSX, generated by PHPExcel.")
                                            ->setKeywords("office 2007 openxml php")
                                            ->setCategory("Test result file");
                $objPHPExcel1->getActiveSheet()->setTitle('Export Result');
                $objPHPExcel1->setActiveSheetIndex(0)
                            ->setCellValue('A1', 'keyword')
                            ->setCellValue('B1', 'landing_page')
                            ->setCellValue('C1', 'Col_1')
                            ->setCellValue('D1', 'Col_2')
                            ->setCellValue('E1', 'quantity')
                            ->setCellValue('F1', 'type');
                $row = 2;
                $a_key = 0;
                foreach($cmts as $c){
                    $a_txt .= preg_replace($pattern,"", (String) $c->content);
                    if (strlen($c->content) > 100) {
                        $x_cmts = preg_replace($pattern,"", (String) $c->content)." ".$this->anchor[$a_key];
                        $objPHPExcel1->setActiveSheetIndex(0)
                                ->setCellValueByColumnAndRow(0, $row, $anc["keyword"][$a_key])
                                ->setCellValueByColumnAndRow(1, $row, $anc["landpage"][$a_key])
                                ->setCellValueByColumnAndRow(2, $row, $title)
                                ->setCellValueByColumnAndRow(3, $row, $x_cmts)
                                ->setCellValueByColumnAndRow(4, $row, 1)
                                ->setCellValueByColumnAndRow(5, $row, 8);
                        $row++;
                    }
                    $a_txt .= " ".$this->anchor[$a_key];
                    $a_txt .="|";
                    $a_key++;
                    if ($a_key >= count($this->anchor)) $a_key = 0;
                }
                $a_txt = rtrim($a_txt,"|");
                $a_txt .= "}";
                $s = mb_convert_encoding($a_txt, 'UTF-8');
                file_put_contents("uploads/anchor_".$a_id.".txt", $s);
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel1, 'Excel2007');
                $objWriter->save("uploads/Anchor_".$a_id.".xlsx");
                $efile = new ExportedFileLink();
                $efile->link = "uploads/Anchor_".$a_id.".xlsx";
                $efile->title = $title;
                $efile->article_id = $a_id;
                $efile->type = false;
                $efile->isAnchor = true;
                $efile->save();
            } else {
                Log::info("Upload Error!");
            }
        }
        $this->layout->nest("content","uploadresult",['c_id'=>$a_id]);
    }
    
    public function postFileCnn(){
        $t_id = Input::get('t_id');
        $destinationPath = 'uploads/';
        $filename = Input::file('upfile')->getClientOriginalName();
        $extension =Input::file('upfile')->getClientOriginalExtension();
        if ($extension != "xls" && $extension != "xlsx") {
            Log::info("You must upload an Excel file");
        } else {
            $uploadSuccess = Input::file('upfile')->move($destinationPath, $filename);
            if ($uploadSuccess) {
                $objReader = new PHPExcel_Reader_Excel2007();
                $objReader->setReadDataOnly(true);
                $objPHPExcel = $objReader->load($destinationPath.$filename);
                $sheetData = $objPHPExcel->getSheet(0)->toArray();
                $anc = ["keyword"=>[],"landpage"=>[]];
                foreach($sheetData as $s){
                    $this->anchor[] = "<a href='".$s[1]."'>".$s[0]."</a>";
                    $anc["keyword"][] = $s[0];
                    $anc["landpage"][] = $s[1];
                }
                $cmts = CnnComment::where('thread_id','=',$t_id)->get();
                $title = $cmts[0]->thread_title;
                $a_txt = "{";
                $pattern = "/(\n|\t|\/|\\|\"|\'|\?|%|@|\*|#|$|&)*/";
                $objPHPExcel1 = new PHPExcel();
                $objPHPExcel1->getProperties()->setCreator("ExportResult")
                                            ->setLastModifiedBy("Khanh Truong")
                                            ->setTitle("Office 2007 XLSX Test Document")
                                            ->setSubject("Office 2007 XLSX Test Document")
                                            ->setDescription("Test doc for Office 2007 XLSX, generated by PHPExcel.")
                                            ->setKeywords("office 2007 openxml php")
                                            ->setCategory("Test result file");
                $objPHPExcel1->getActiveSheet()->setTitle('Export Result');
                $objPHPExcel1->setActiveSheetIndex(0)
                            ->setCellValue('A1', 'keyword')
                            ->setCellValue('B1', 'landing_page')
                            ->setCellValue('C1', 'Col_1')
                            ->setCellValue('D1', 'Col_2')
                            ->setCellValue('E1', 'quantity')
                            ->setCellValue('F1', 'type');
                $row = 2;$a_key = 0;
                foreach($cmts as $c){
                    $a_txt .= preg_replace($pattern,"", (String) $c->comment_message);
                    if (strlen($c->comment_message) > 100) {
                        $x_cmts = preg_replace($pattern,"", (String) $c->comment_message)." ".$this->anchor[$a_key];
                        $objPHPExcel1->setActiveSheetIndex(0)
                                ->setCellValueByColumnAndRow(0, $row, $anc["keyword"][$a_key])
                                ->setCellValueByColumnAndRow(1, $row, $anc["landpage"][$a_key])
                                ->setCellValueByColumnAndRow(2, $row, $title)
                                ->setCellValueByColumnAndRow(3, $row, $x_cmts)
                                ->setCellValueByColumnAndRow(4, $row, 1)
                                ->setCellValueByColumnAndRow(5, $row, 8);
                        $row++;
                    }
                    $a_txt .= " ".$this->anchor[$a_key];
                    $a_txt .="|";
                    $a_key++;
                    if ($a_key >= count($this->anchor)) $a_key = 0;
                }
                $a_txt = rtrim($a_txt,"|");
                $a_txt .= "}";
                $s = mb_convert_encoding($a_txt, 'UTF-8');
                file_put_contents("uploads/anchor_".$t_id.".txt", $s);
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel1, 'Excel2007');
                $objWriter->save("uploads/Anchor_".$t_id.".xlsx");
                $efile = new ExportedFileLink();
                $efile->link = "uploads/Anchor_".$t_id.".xlsx";
                $efile->title = $title;
                $efile->article_id = $t_id;
                $efile->type = true;
                $efile->isAnchor=true;
                $efile->save();
            } else {
                Log::info("Upload Error!");
            }
        }
        $this->layout->nest("content","uploadresult",['c_id'=>$t_id]);
    }
    
//    public function getDucExport($c_id){
//        return Response::download("uploads/ducoutput_".$c_id.".txt");
//    }
    
    public function getAnchorExport($c_id){
        return Response::download("uploads/anchor_".$c_id.".txt");
    }
    
    public function getAnchorExcel($c_id){
        return Response::download("uploads/Anchor_".$c_id.".xlsx");
    }

    public function getExport($a_id){
        $cmts = CrawledComment::where('article_id','=',$a_id)->get();
        $data = "{";
        $pattern = "/(\n|\t|\/|\\|\"|\'|\?|%|@|\*|#|$|&)*/";
        foreach($cmts as $c){
            $data .= preg_replace($pattern,"", (String) $c->content);
            $data .="|";
        }
        $data = rtrim($data,"|");
        $data .= "}";
        $s = mb_convert_encoding($data, 'UTF-8');
        file_put_contents("uploads/output_".$a_id.".txt", $s);
        return Response::download("uploads/output_".$a_id.".txt");
    }
    
    public function getExportCnn($t_id){
        $cmts = CnnComment::where('thread_id','=',$t_id)->get();
        $data = "{";
        $pattern = "/(\n|\t|\/|\\|\"|\'|\?|%|@|\*|#|$|&)*/";
        foreach($cmts as $c){
            $data .= preg_replace($pattern,"", (String) $c->comment_message);
            $data .="|";
        }
        $data = rtrim($data,"|");
        $data .= "}";
        $s = mb_convert_encoding($data, 'UTF-8');
        file_put_contents("uploads/output_".$t_id.".txt", $s);
        return Response::download("uploads/output_".$t_id.".txt");
    }
//    public function DucExport($c_id){
//        $cmts = CrawledComment::where('category_id','=',$c_id)->get();
//        $data = "";
//        $pattern = "/(\n|\t|\/|\\|\"|\'|\?|%|@|\*|#|$|&)*/";
//        foreach($cmts as $c){
//            $data .= preg_replace($pattern,"", (String) $c->content);
//            $data .="\n";
//        }
//        $reg = "/(<br>|<)+/";
//        $d = preg_replace($reg, " ", $data);
//        $s = mb_convert_encoding($d, 'UTF-8');
//        file_put_contents("uploads/ducoutput_".$c_id.".txt", $s);
//    }
    
    public function getExportExcel($a_id){
        $data = CrawledComment::where('article_id','=',$a_id)->get();
        $cmts=[];
        $title = explode("-",$data[0]->title)[0];
        $pattern = "/(\n|\t|\/|\\|\"|\'|\?|%|@|\*|#|$|&)*/";
        foreach($data as $d){
            if (strlen($d->content) > 100) {
                $cmts[] = preg_replace($pattern,"", (String) $d->content);
            }
        }
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("ExportResult")
                                    ->setLastModifiedBy("Khanh Truong")
                                    ->setTitle("Office 2007 XLSX Test Document")
                                    ->setSubject("Office 2007 XLSX Test Document")
                                    ->setDescription("Test doc for Office 2007 XLSX, generated by PHPExcel.")
                                    ->setKeywords("office 2007 openxml php")
                                    ->setCategory("Test result file");
        $objPHPExcel->getActiveSheet()->setTitle('Export Result');
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'keyword')
                    ->setCellValue('B1', 'landing_page')
                    ->setCellValue('C1', 'Col_1')
                    ->setCellValue('D1', 'Col_2')
                    ->setCellValue('E1', 'quantity')
                    ->setCellValue('F1', 'Type');
        for($row=2;$row<count($cmts)+2;$row++){
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueByColumnAndRow(0, $row, "")
                        ->setCellValueByColumnAndRow(1, $row, "")
                        ->setCellValueByColumnAndRow(2, $row, $title)
                        ->setCellValueByColumnAndRow(3, $row, $cmts[$row-2])
                        ->setCellValueByColumnAndRow(4, $row, 1)
                        ->setCellValueByColumnAndRow(5, $row, 8);
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save("uploads/output_".$a_id.".xlsx");
        $efile = new ExportedFileLink();
        $efile->link = "uploads/output_".$a_id.".xlsx";
        $efile->title = $title;
        $efile->article_id = $a_id;
        $efile->type = false;
        $efile->isAnchor = false;
        $efile->save();
        return Response::download("uploads/output_".$a_id.".xlsx");
    }
    
    public function getExportExcelCnn($t_id){
        $data = CnnComment::where('thread_id','=',$t_id)->get();
        $cmts=[];
        $title = $data[0]->thread_title;
        $pattern = "/(\n|\t|\/|\\|\"|\'|\?|%|@|\*|#|$|&)*/";
        foreach($data as $d){
            if (strlen($d->comment_message) > 100) {
                $cmts[] = preg_replace($pattern,"", (String) $d->comment_message);
            }
        }
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("ExportResult")
                                    ->setLastModifiedBy("Khanh Truong")
                                    ->setTitle("Office 2007 XLSX Test Document")
                                    ->setSubject("Office 2007 XLSX Test Document")
                                    ->setDescription("Test doc for Office 2007 XLSX, generated by PHPExcel.")
                                    ->setKeywords("office 2007 openxml php")
                                    ->setCategory("Test result file");
        $objPHPExcel->getActiveSheet()->setTitle('Export Result');
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Col_1')
                    ->setCellValue('B1', 'Col_2')
                    ->setCellValue('C1', 'quantity')
                    ->setCellValue('D1', 'type');
        for($row=2;$row<count($cmts);$row++){
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueByColumnAndRow(0, $row, $title)
                        ->setCellValueByColumnAndRow(1, $row, $cmts[$row])
                        ->setCellValueByColumnAndRow(2, $row, 1)
                        ->setCellValueByColumnAndRow(3, $row, 8);
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save("uploads/output_".$t_id.".xlsx");
        $efile = new ExportedFileLink();
        $efile->link = "uploads/output_".$t_id.".xlsx";
        $efile->title = $title;
        $efile->article_id = $t_id;
        $efile->type = true;
        $efile->isAnchor = false;
        $efile->save();
        return Response::download("uploads/output_".$t_id.".xlsx");
    }

    public function updateComment($URL) {
        $link = $URL;
        $data = array();
        if (!is_null($link)) {
            $content = file_get_html($link);
            $elems = $content->find("//div/div/div/script[2]");
            if ($elems == NULL) return;// echo $elems[];
            $part = strstr($elems[0], "objectid:");
            $objectIdPart = str_split($part, 9)[1];
            $objectId = intval($objectIdPart);
            $part = strstr($elems[0], "objecttype:");
            $objectTypePart = str_split($part, 11)[1];
            $objectType = intval($objectTypePart);
            $jsonURL = "http://interactions.vnexpress.net/index/get?offset=0&limit=1000&frommobile=0&sort=like&objectid=" . $objectId . "&objecttype=" . $objectType . "&siteid=1000000";
            $json = file_get_contents($jsonURL);
            $json = substr($json, 16);
            $json = rtrim($json, ";");
            $json = rtrim($json, ")");
            $json = json_decode($json);
            foreach ($json->data->items as $element) {
                $comment = new CrawledComment;
                $comment->article_id = $element->article_id;
                $comment->comment_id = $element->comment_id;
                $comment->title = $element->title;
                $comment->content = $element->content;
                $comment->full_name = $element->full_name;
                $comment->userlike = $element->userlike;
                $comment->parent_id = $element->parent_id;
                $comment->save();
                if ($element->replys->total != 0) {
                    $reply = new CrawledComment;
                    $reply->article_id = $element->replys->items[0]->article_id;
                    $reply->comment_id = $element->replys->items[0]->comment_id;
                    $reply->title = $element->replys->items[0]->title;
                    $reply->content = $element->replys->items[0]->content;
                    $reply->full_name = $element->replys->items[0]->full_name;
                    $reply->userlike = $element->replys->items[0]->userlike;
                    $reply->parent_id = $element->replys->items[0]->parent_id;
                    $reply->save();
                    if ($element->replys->total > 1) {
                        $reply = new CrawledComment;
                        $reply->article_id = $element->replys->items[1]->article_id;
                        $reply->comment_id = $element->replys->items[1]->comment_id;
                        $reply->title = $element->replys->items[1]->title;
                        $reply->content = $element->replys->items[1]->content;
                        $reply->full_name = $element->replys->items[1]->full_name;
                        $reply->userlike = $element->replys->items[1]->userlike;
                        $reply->parent_id = $element->replys->items[1]->parent_id;
                        $reply->save();
                    }
                    if ($element->replys->total > 2) {
                        $replyURL = "http://interactions.vnexpress.net//index/getreplay?siteid=1002592&objectid=" . $objectId . "&objecttype=" . $objectType . "&id=" . $element->comment_id . "&limit=100&offset=2";
                        $replyJson = file_get_contents($replyURL);
                        $replyJson = substr($replyJson, 16);
                        $replyJson = rtrim($replyJson, ";");
                        $replyJson = rtrim($replyJson, ")");
                        $replyJson = json_decode($replyJson);
                        foreach ($replyJson->data->items as $element1) {
                            $reply = new CrawledComment;
                            $reply->article_id = $element1->article_id;
                            $reply->comment_id = $element1->comment_id;
                            $reply->title = $element1->title;
                            $reply->content = $element1->content;
                            $reply->full_name = $element1->full_name;
                            $reply->userlike = $element1->userlike;
                            $reply->parent_id = $element1->parent_id;
                            $reply->save();
                        }
                    }
                }
            }
        }
    }
    public function getCrawler() {
        $html = file_get_html("http://vnexpress.net/");
        $ul = $html->find('ul[id=menu_web]')[0];
        $a = $ul->find('a');
        $hrefs = [];
        foreach ($a as $category) {
            if ($category->href[0] == '/')
                $category->href = 'http://vnexpress.net' . $category->href;
            if ($category->href[0] == "h") {
                $hrefs[] = $category->href;
            }
        }
        foreach ($hrefs as $h) {
            if ($h != "http://vnexpress.net") {
                $html1 = file_get_html($h);
                $linkcrwl = $html1->find("ul[id=news_home] a[class=icon_commend]");
                foreach ($linkcrwl as $l)
                    CommentListController::updateComment($l->href);
            }
        }
        $data = [];
        $data['count'] = count($hrefs);
        $this->layout->nest("content", "comment", $data);
    }

}
