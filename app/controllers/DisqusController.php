<?php

class DisqusController extends BaseController {
    
    public $layout = "layouts.admin";

    public function index(){
        $data = array();
        $this->layout->nest("content", "home.index", $data);
    }

    /**
     * Get the submitted url and insert to pending table
     *
     * @return str
     */
    public function postInsertDisqusLinkToDatabase() {
        $url = Input::get("URL");
        $pendingLink = new PendingDisqusComment;
        $pendingLink->thread_link = $url;
        try{
            $pendingLink->save();
        }catch(Illuminate\Database\QueryException $ex){
            Log::info($ex);
            if(strstr($ex->getMessage(), "Duplicate") != false)
            {
                $pendingLink = PendingDisqusComment::where("thread_link",$url)->get()[0];
                if($pendingLink->status){
                    return "reset";
                }else{
                    return "That link is added and waiting for processing already";
                }
            }else{
                return "Something wrong happened, please contact your administrator";
            }
        }catch(ErrorException $ex){
            Log::info($ex);
            return "Something wrong happened, please contact your administrator";
        }
        return "Link: ".$url." is successfully inserted to pending list";
    }


    /**
     * Reset status of a link to get comment from it again
     *
     * @return str
     */
    public function postResetStatus(){
        $url = Input::get("URL");
        try {
            $pendingLink = PendingDisqusComment::where("thread_link",$url)->get()[0];
            $pendingLink->status = false;
            $pendingLink->save();
            return "Link ".$url." is successfully reseted to get more comment";
        } catch (ErrorException $ex) {
            Log::info($ex);
            return "Something wrong happened, please contact your administrator";
        }
    }
}
