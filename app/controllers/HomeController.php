<?php

class HomeController extends BaseController {

    protected $forum_type;
    public $layout = "layouts.admin";

    public function index() {
        $data = array();
        $this->layout->nest("content", "home.index", $data);
    }

    public function postInsertVneLinkToDatabase() {
        $this->url = Input::get("URL");
        $link = $this->url;
        $checkLink = strstr($link,"vnexpress.net");
        if($checkLink == NULL) return "Link: ".$link." is not a Vnexpress's thread, please try again";
        $content = file_get_html($link);
        //$this->info("Processing: " . $link);
        $tagList = [];
        foreach ($content->find('a[class=tag_item]') as $tag) {
            $tagList[] = $tag->plaintext;
        }

        $elems = $content->find("//div/div/div/script[2]");
        if ($elems == NULL)
            return;
        //$this->info("found page info");
        $part = strstr($elems[0], "objectid:");
        if ($part == false)
            return;
        $objectIdPart = str_split($part, 9)[1];
        $objectId = intval($objectIdPart);
        $part = strstr($elems[0], "objecttype:");
        $objectTypePart = str_split($part, 11)[1];
        $objectType = intval($objectTypePart);
        $part = strstr($elems[0], "categoryid:");
        $categoryIdPart = str_split($part, 11)[1];
        $categoryId = intval($categoryIdPart);
        $table = new VnexpressPendingComment();
        $table->thread_url = $link;
        $table->category_id = $categoryId;
        $table->article_id = $objectId;
        $table->object_type = $objectType;
        $table->title = $content->find('div[class=title_news]')[0]->plaintext;
        try {
            $table->save();
            return "Link: " . $link . " is successfully inserted to pending list";
        } catch (\Illuminate\Database\QueryException $ex) {
            //$this->info($ex);
            return "That link is added already";
        };
    }
    public function postReloadThread(){
        $thrd_url = Input::get(URL);
        $reloadLink = VnexpressPendingComment::where('thread_url', $thrd_url)->get();
        $reloadLink->status = false;
        $reloadLink->save();
        return "Link: " . $thrd_url . " is successfully inserted to pending list";
    }
}
