<?php

class HtmlValidatorController extends BaseController {

    public $layout = "layouts.admin";

    public function getInput() {
        $data = array();
        $this->layout->nest("content", "input", $data);
    }

    public function getResult() {
        $link = Input::get('URL');
        $validateUrl = 'http://validator.w3.org/check?uri=' . $link;
        $useAgent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13';
        $fp = fopen("validate.html", "w");
        $ch = curl_init($validateUrl);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, $useAgent);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
        $data = array();
        $data['link'] = $link;
        $this->layout->nest('content', 'validationpage', $data);
    }

}
