<?php

class SpamController extends BaseController {

    public $layout = "layouts.admin";
    public function postToDatabase(){
        $thread_id = Input::get("a");
        $table = new Pendingwp();
        $table->thread_id = $thread_id;
        try{
            $table->save();
        }
        catch(\Illuminate\Database\QueryException $ex){
            return "That link is already added to Pending list";
        }
        return "That link is successfully added to Pending list";
        //$data = array();
        //$this->layout->nest("content", "spam.index", $data);
    }
    public function postRepostToDatabase(){
        $thread_id = Input::get("a");
        Log::info($thread_id);
        $table = Pendingwp::where('thread_id',$thread_id)->get();
        $table[0]->status = false;
        Log::info($table);
        try{
            $table[0]->save();
        }
        catch(\Illuminate\Database\QueryException $ex){
            return "Unknow Error";
        };
        return "That link is successfully updated";
    }

    public function getIndex() {
        $data = array();
        $this->layout->nest("content", "spam.index", $data);
    }
}

//EOF