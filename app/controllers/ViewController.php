<?php

class ViewController extends \BaseController {

    protected $layout = "layouts.admin";
    public function getView(){
        $query = PendingDisqusComment::all();
        $result=["title"=>[],"link"=>[],"id"=>[],"status"=>[],"date"=>[],"total"=>[]];
        foreach ($query as $q){
            if ($q->status){
                $result["title"][] = $q->thread_title;
                $result["link"][] = $q->thread_link;
                $result["id"][] = $q->thread_id;
                $result["status"][] = $q->status;
                $result["date"][] = $q->updated_at;
                $result["total"][] = count(CnnComment::where("thread_id","=",$q->thread_id)->get());
            }else {
                $result["title"][] = NULL;
                $result["link"][] = $q->thread_link;
                $result["id"][] = NULL;
                $result["status"][] = $q->status;
                $result["date"][] = "__/__/____ __:__:__";
                $result["total"][] = 0;
            }
        }
        $this->layout->nest("content","show.view",["result"=>$result]);
    }
    public function getViewVne(){
        $query = VnexpressPendingComment::all();
        $result=["title"=>[],"link"=>[],"id"=>[],"status"=>[],"date"=>[],"total"=>[]];
        foreach ($query as $q){
            if ($q->status){
                $result["title"][] = $q->title;
                $result["link"][] = $q->thread_url;
                $result["id"][] = $q->article_id;
                $result["status"][] = $q->status;
                $result["date"][] = $q->updated_at;
                $result["total"][] = count(CrawledComment::where("article_id","=",$q->article_id)->get());
            }else {
                $result["title"][] = NULL;
                $result["link"][] = $q->thread_url;
                $result["id"][] = NULL;
                $result["status"][] = $q->status;
                $result["date"][] = "__/__/____ __:__:__";
                $result["total"][] = 0;
            }
        }
        $query2 = Pendingwp::all();
        $result2 = ['thread_id'=>[],'status'=>[],'count'];
        $count = 0;
        foreach ($query2 as $q2){
            $result2['thread_id'][] = $q2->thread_id;
            $result2['status'][] = $q2->status;
            $count++;
        }
        $result2['count'] = $count;
        $this->layout->nest("content","show.viewvne",["result"=>$result, 'result2'=>$result2]);
    }

    public function getViewListFile(){
        $query = ExportedFileLink::paginate(20);
        $this->layout->nest("content","show.listfile",['result' => $query]);
    }

    public function getDownFile($dpath,$fname){
        return Response::download($dpath."/".$fname);
    }

    public function getEditFile($dpath,$fname){
        $link = $dpath."/".$fname;
        Log::info($link);
        $objReader = new PHPExcel_Reader_Excel2007();
        $objReader->setReadDataOnly();
        $objExcel = $objReader->load($link);
        $sheetData = $objExcel->getSheet(0)->toArray();
        $this->layout->nest("content","show.editfile",["data"=>$sheetData,"dpath"=>$dpath,"fname"=>$fname]);
    }

    public function postSave(){
        $dpath = Input::get("dpath");
        $fname = Input::get("fname");
        $row = Input::get("row");
        $objPHPExcel = PHPExcel_IOFactory::load($dpath."/".$fname);
        $objPHPExcel->getProperties()->setCreator("ExportResult")
                                    ->setLastModifiedBy("Khanh Truong")
                                    ->setTitle("Office 2007 XLSX Test Document")
                                    ->setSubject("Office 2007 XLSX Test Document")
                                    ->setDescription("Test doc for Office 2007 XLSX, generated by PHPExcel.")
                                    ->setKeywords("office 2007 openxml php")
                                    ->setCategory("Test result file");
        $objPHPExcel->getActiveSheet()->setTitle('Export Result');
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValueByColumnAndRow(0, $row, Input::get("keyword"))
                    ->setCellValueByColumnAndRow(1, $row, Input::get("landpage"))
                    ->setCellValueByColumnAndRow(2, $row, Input::get("title"))
                    ->setCellValueByColumnAndRow(3, $row, Input::get("content"))
                    ->setCellValueByColumnAndRow(4, $row, Input::get("quantity"))
                    ->setCellValueByColumnAndRow(5, $row, Input::get("type"));
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($dpath."/".$fname);
        return Redirect::back();
    }
}
