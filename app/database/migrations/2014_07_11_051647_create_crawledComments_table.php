<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrawledCommentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('crawled_comments', function($table) {
                    $table->increments('id');
                    $table->integer('category_id');
                    $table->string('thread_url');
                    $table->integer('article_id');
                    $table->integer('comment_id');
                    $table->string('title')->nullable();
                    $table->text('content');
                    $table->string('full_name')->nullable();
                    $table->integer('userlike');
                    $table->integer('parent_id');
                    $table->timestamps();
                    //$table->primary(array('article_id','comment_id'));
                    $table->unique(array('comment_id'));
                    //$table->nullable(array('title','full_name'));
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('crawled_comments');
    }

}
