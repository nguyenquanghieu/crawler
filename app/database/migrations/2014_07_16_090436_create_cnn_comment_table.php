<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCnnCommentTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('cnn_comments', function($table) {
            $table->increments('id');
            $table->timestamps();
            //thread
            $table->string('thread_id');
            $table->string('thread_link');
            $table->string('thread_title')->default('unknow title');
            //author
            $table->string('author_name')->nullable();
            $table->string('author_profile_url')->nullable();
            $table->double('author_reputation')->nullable();
            //comment
            $table->integer('comment_id');
            $table->integer('comment_parent')->nullable();
            $table->integer('comment_likes');
            $table->integer('comment_dislike');
            $table->text('comment_message');
            //constraints
            $table->unique(array('comment_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('cnn_comments');
    }

}
