<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendingDisqusCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("pending_disqus_comments",function($table)
		{
			$table->increments('id');
            $table->timestamps();

            $table->string('thread_id')->nullable();
            $table->string('thread_link');
            $table->string('thread_title')->nullable();
            $table->boolean('status')->default(false); //true: done

            //constraints
            $table->unique(array('thread_link'));
		});
	}
	public function down()
	{
		Schema::drop('pending_disqus_comments');
	}

}
