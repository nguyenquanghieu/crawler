<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVnexpressPendingCommentsTable extends Migration {
	public function up()
	{
            Schema::create('vnexpress_pending_comments',  function ($table){
                $table->increments('id');
                $table->string('thread_url');
                $table->string('title');
                $table->integer('category_id');
                $table->integer('article_id');
                $table->integer('object_type');
                $table->boolean('status')->default(false);
                $table->timestamps();
                $table->unique(array('thread_url'));
            });
	}
	public function down()
	{
		Schema::drop('vnexpress_pending_comments');
	}

}
