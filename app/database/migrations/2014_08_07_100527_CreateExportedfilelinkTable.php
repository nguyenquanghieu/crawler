<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExportedfilelinkTable extends Migration {

	public function up()
	{
            Schema::create('exported_file_links',  function ($table){
                $table->increments('id');
                $table->string('link');
                $table->string('title');
                $table->integer('article_id');
                $table->boolean('type');
                $table->boolean('isAnchor');
                $table->timestamps();
            });
	}
	public function down()
	{
		Schema::drop('exported_file_links');
	}

}
