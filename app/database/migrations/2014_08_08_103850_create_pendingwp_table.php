<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendingwpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pendingwps',  function ($table){
                    $table->increments('id');
                    $table->string('thread_id');
                    $table->boolean('status')->default(false);
                    $table->timestamps();
                    $table->unique(array('thread_id'));
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pendingwps');
	}

}
