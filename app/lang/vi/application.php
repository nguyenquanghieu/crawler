<?php

return array(
    'application name' => 'MangoAds Internship v2',
    'dashboard' => 'Bảng điều khiển',
    'post management' => "Quản lý bài viết",
    'checksite' => "Kiểm tra trang web của bạn",
    'viewsite' => "Xem các link disqus",
    'searchvne' => "Tìm kiếm VnExpress",
    'searchcnn' => "Tìm kiếm CNN",
    'search' => "Tìm kiếm",
    "excelexport" => "Upload file Anchor Text",
    "viewsitevne" => "Xem các link VnExpress",
    "getcommentfromlink" => "Lấy comment từ link",
    "spam" => "Đăng comment"
);
