<?php

return array(
    'label' => array(
        'view' => "Xem Comment",
        'exporttxt' => "Tải về file txt",
        'anchortxt' => "Tải về file txt(kèm Anchor text)",
        'anchorxls' => "Tải về file Excel(có Anchor text)",
        'exportxls' => "Tải về file Excel(không có Anchor text)",
        'viewfile' => "Chỉnh sửa Exported File",
        'downfile' => "Tải về",
    ),
    'menu' => array(
        'list' => "Danh sách bài viết",
        'create' => "Tạo mới bài viết",
    ),
    'title' => array(
        'list' => "Danh sách bài viết",
        'create' => "Tạo mới bài viết",
    ),
    'title.list'=>'Nguyễn Quang Hiếu',
);
