<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

Route::controller("home", "HomeController");
Route::get('/', "HomeController@index");
Route::controller('disqus','DisqusController');
Route::controller('html-validator',  'HtmlValidatorController');
Route::controller('comment-list','CommentListController');
Route::controller('disqus','DisqusController');
Route::controller('pending','ViewController');
Route::controller('spam','SpamController');
