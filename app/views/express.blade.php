@section('content')

<?php
echo Form::open(array(
    'method'=>"GET",
    'url'=> URL::action("CommentListController@getSearchExpress")
));
?>
<fieldset>
    <div class="form-search">
        <!--<label for=" ">Enter keyword: </label>-->
        <input type="text" class="input-medium search-query" name ="keyword" placeholder="Enter Keyword here!">
        <span class="info">
            <select name="op">
                <option value="0" selected="true">Only Title</option>
                <option value="1">Both title and content</option>
            </select>
        </span>
        <input type ="submit" class="btn btn-facebook" value="Search">
    </div>
</fieldset>
<?php
echo Form::close();
if (isset($result)){
    $total = count($result["article_id"]);
    if ($total > 0){
        echo "<span>Có tổng cộng ".$total." kết quả được tìm thấy!</span>";
        echo "<div class='info'>";
            echo "<table class='table table-bordered'>";
                echo "<thead>";
                    echo "<tr class='success'>";
                        echo "<th>Tiêu đề</th>";
                        echo "<th>Ngày lấy</th>";
                        echo "<th>Số Comment</th>";
                    echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                for($i = 0;$i<$total;$i++){
                    echo "<tr>";
                        echo "<td><a href='".URL::action("CommentListController@getArticle",array("id"=>$result["article_id"][$i]))."'>".$result["title"][$i]."</a></td>";
                        echo "<td>".$result["updated_at"][$i]."</td>";
                        echo "<td>".$result["total"][$i]."</td>";
                    echo "</tr>";
                }
                echo "</tbody>";
            echo "</table>";
        echo "</div>";
    }else{
        echo "<span>Có tổng cộng ".$total." kết quả được tìm thấy!</span>";
    }
}
?>
@stop