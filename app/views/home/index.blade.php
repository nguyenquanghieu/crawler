@section("content")
<?php
echo Form::open(array(
    'method' => 'post',
    'url' => URL::action('DisqusController@postInsertDisqusLinkToDatabase')
))
?>
<fieldset>
    <div class="form-group">
        <label classfor=" ">Enter URL: </label>
        <input type="text" class="form-control" name ="URL" id="url" placeholder="Enter url">
    </div>
    <div class="form-group">
        <label for = " ">Forum type:</label>
        <select class="form-control" id ="forum_type">
            <option value = "Disqus">Disqus</option>
            <option value = "Vnexpress">Vnexpress</option>
        </select>
    </div>
    <!-- Button -->
    <div class="control-group">
        <label class="control-label" for="Enter"></label>
        <div class="controls">
            <a id="getComment" class="btn btn-primary">Add to pending list</a>
        </div>
    </div>
</fieldset>
<br>
<?php
echo Form::close();
?>
<div id="dialog-confirm"></div>
<div id="spinner" class="spinner" style="display:none;">
    <img id="img-spinner" src="<?php echo asset('spinner.gif'); ?>" alt="Loading"/>
</div>
<textarea id="result" style="width: 1047px; height: 200px;"></textarea>
@stop

@section("scripts")
<script type="text/javascript">

    $("#getComment").click(function() {
        $('#spinner').show();
        var url = $("#url").val();
        var forum_type = $("#forum_type").val();
        var postData = {"URL": url};
        $("#result").text("");
        if (forum_type == "Disqus") {
            $.post('<?php echo URL::action("DisqusController@postInsertDisqusLinkToDatabase"); ?>',
                    postData,
                    function(data) {
                        if(data == "reset")
                        {
                            $('#spinner').hide();
                            var reset = confirm("That link is added and processed, do you want it to be processed again?");
                            if(reset)
                            {
                                $.post('<?php echo URL::action("DisqusController@postResetStatus"); ?>',
                                postData,
                                function(resetData) {
                                    $("#result").val(resetData);
                                    $('#spinner').hide();
                                });
                            }
                        }else{
                            $("#result").val(data);
                            $('#spinner').hide();    
                        }
                    }
            );
        } else {
            $.post('<?php echo URL::action("HomeController@postInsertVneLinkToDatabase"); ?>',
                    postData,
                    function(data) {
                        $("#result").text(data);
                        if (data == "That link is added already") {
                         //   var confrm = confirm("Do you want to reload this thread?");
                            if (confirm("That link is added and processed, do you want it to be processed again?") ==  true) {
                                $.post('<?php echo URL::action("HomeController@postReloadThread"); ?>',
                                        postData,
                                        function(data1) {
                                            console.log(data1);
                                            $("#result").text(data1);
                                        }
                                );
                            }
                        }
                        $('#spinner').hide();

                    }
            );
        }
    });

    $(document).ajaxError(function() {
        $('#spinner').hide();
    });
</script>
@stop