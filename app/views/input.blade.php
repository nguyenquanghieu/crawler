@section('content')
<?php
echo Form::open(array(
    'method' => 'get',
    'url' => URL::action('HtmlValidatorController@getResult')
))
?>
<fieldset>
    <!-- Form Name -->
    <legend>W3school HTML Validator</legend>

    <!-- Text input-->
    <div class="form-group">
        <label for=" ">Enter URL:</label>
        <input type="text" class="form-control" name ="URL" placeholder="Enter url">
    </div>

    <!-- Button -->
    <div class="control-group">
        <label class="control-label" for="Enter"></label>
        <div class="controls">
            <button type ="submit" class="btn btn-primary">Check!</button>
        </div>
    </div>
</fieldset>
<?php
echo Form::close();
echo "<br><br><strong>Hướng dẫn: </strong> Nhập đường dẫn trang web của bạn vào mục Enter URL, sau đó nhấn nút check và chờ kết quả";
?>
@stop