<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo isset($title) ? $title : trans('application.application name'); ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?php echo asset('lte/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo asset('lte/css/bootstrap-fileinput.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo asset('lte/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
        <!-- Ionicons -->
        <link href="<?php echo asset('lte/css/ionicons.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo asset('lte/css/iCheck/all.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo asset('lte/css/datatables/dataTables.bootstrap.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?php echo asset('lte/css/morris/morris.css') ?>" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="<?php echo asset('lte/css/jvectormap/jquery-jvectormap-1.2.2.css') ?>" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="<?php echo asset('lte/css/fullcalendar/fullcalendar.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="<?php echo asset('lte/css/daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?php echo asset('lte/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo asset('codemirror/lib/codemirror.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo asset('codemirror/theme/neo.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo asset('datetimepicker/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo asset('lte/css/AdminLTE.css') ?>" rel="stylesheet" type="text/css" />
        @yield('styles')
    </head>
    <body class="skin-blue">
        @if(!isset($isFullScreen)||!$isFullScreen)
        <header class="header">
            <a href="<?php echo URL::to("/") ?>" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                {{trans('application.application name')}}
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                    </ul>
                </div>
            </nav>
        </header>
        @endif
        <div class="wrapper row-offcanvas row-offcanvas-left">
            @if(!isset($isFullScreen)||!$isFullScreen)
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo Gravatar::src("tabenguyen@gmail.com", 42) ?>" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info messages-menu">
                            <p>Hello!</p>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="<?php echo Active::controller('Home'); ?>">
                            <a href="<?php echo URL::to("/") ?>">
                                <i class="fa fa-dashboard"></i> <span>{{trans('application.getcommentfromlink')}}</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo URL::action("ViewController@getView") ?>">
                                <i class="fa fa-dashboard"></i> <span>{{trans('application.viewsite')}}</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo URL::action("ViewController@getViewVne") ?>">
                                <i class="fa fa-dashboard"></i> <span>{{trans('application.viewsitevne')}}</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo URL::action("ViewController@getViewListFile") ?>">
                                <i class="fa fa-dashboard"></i> <span>{{trans('post.label.viewfile')}}</span>
                            </a>
                        </li>
                        <li class="treeview <?php echo Active::controller('Post'); ?>">
                            <a href="#">
                                <i class="fa fa-dashboard"></i>
                                <span>{{trans('application.search')}}</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="<?php echo URL::action("CommentListController@getCnn")?>">
                                        <i class="fa fa-angle-double-right"></i> <span>{{trans('application.searchcnn')}}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo URL::action("CommentListController@getExpress")?>">
                                        <i class="fa fa-angle-double-right"></i> <span>{{trans('application.searchvne')}}</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo URL::action("SpamController@getIndex") ?>">
                                <i class="fa fa-dashboard"></i> <span>{{trans('application.spam')}}</span>
                            </a>
                        </li>
                    </ul>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                </section>
                <!-- /.sidebar -->
            </aside>
            @endif
            <aside class="right-side {{(isset($isFullScreen) && $isFullScreen) ? 'strech' : ''}}">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php echo isset($title) ? $title : trans('application.application name'); ?>
                    </h1>
                    <!--                    <ol class="breadcrumb">
                                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                            <li class="active">Dashboard</li>
                                        </ol>-->
                </section>

                <!-- Main content -->
                <section class="content">
                    @yield('content')
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
    </div>
    <!--/Chat Modal--> 

    <!-- jQuery 2.0.2 -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script src="<?php echo asset('js/websocket.js') ?>"></script>
    <!-- jQuery UI 1.10.3 -->
    <script src="<?php echo asset('lte/js/jquery-ui-1.10.3.min.js') ?>" type="text/javascript"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset('lte/js/bootstrap.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo asset('lte/js/bootstrap-fileinput.js') ?>" type="text/javascript"></script>
    <!-- Morris.js charts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?php echo asset('lte/js/plugins/morris/morris.min.js') ?>" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="<?php echo asset('lte/js/plugins/sparkline/jquery.sparkline.min.js') ?>" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="<?php echo asset('lte/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo asset('lte/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>" type="text/javascript"></script>
    <!-- fullCalendar -->
    <script src="<?php echo asset('lte/js/plugins/fullcalendar/fullcalendar.min.js') ?>" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo asset('lte/js/plugins/jqueryKnob/jquery.knob.js') ?>" type="text/javascript"></script>
    <!-- daterangepicker -->
    <script src="<?php echo asset('lte/js/plugins/daterangepicker/daterangepicker.js') ?>" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo asset('lte/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="<?php echo asset('lte/js/plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>
    <!-- CKEditor -->
    <script src="<?php echo asset('lte/js/plugins/ckeditor/ckeditor.js') ?>" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo asset('lte/js/AdminLTE/app.js') ?>" type="text/javascript"></script>
    @yield('scripts')

</body>
</html>