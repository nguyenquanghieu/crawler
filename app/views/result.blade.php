@section('content')
<a class="btn btn-info" href="{{URL::action("CommentListController@getFile",array('a_id'=>$a_id))}}"><span>{{trans('application.excelexport')}}</span></a>
<a class="btn btn-info" href="{{URL::action("CommentListController@getExportExcel",array('a_id'=>$a_id))}}"><span>{{trans("post.label.exportxls")}}</span></a>
<a class="btn btn-info" href="{{URL::action("CommentListController@getExport",array('a_id'=>$a_id))}}"><span>{{trans("post.label.exporttxt")}}</span></a>
<a class="btn btn-link" href="{{URL::action("CommentListController@getBack")}}">Back</a>
<?php
    foreach($cmts as $c){
?>
<div class="row">
    <div class="col-lg-2">
        <?php echo $c->full_name ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-8 well">
        <?php echo $c->content ?>
    </div>
</div>
<?php
    }
    echo $cmts->links();
?>
@stop