@section('content')
<a class="btn btn-info" href="{{URL::action("CommentListController@getFileCnn",array('t_id'=>$t_id))}}"><span>{{trans('application.excelexport')}}</span></a>
<a class="btn btn-info" href="{{URL::action("CommentListController@getExportExcelCnn",array('t_id'=>$t_id))}}"><span>{{trans("post.label.exportxls")}}</span></a>
<a class="btn btn-info" href="{{URL::action("CommentListController@getExportCnn",array('t_id'=>$t_id))}}"><span>{{trans("post.label.exporttxt")}}</span></a>
<a class="btn btn-link" href="{{URL::action("CommentListController@getBack")}}">Back</a>
<?php
    foreach($cmts as $c){
?>
<div class="row">
    <div class="col-lg-2">
        <?php echo $c->author_name ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 well">
        <?php echo $c->comment_message ?>
    </div>
</div>
<?php
    }
    echo $cmts->links();
?>
@stop