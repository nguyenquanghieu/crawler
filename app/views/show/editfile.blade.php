@section("content")
<div class='info'>
    <table class='table table-bordered'>
        <thead>
            <tr class='info'>
                <th>Keyword</th>
                <th>Landing Page</th>
                <th>Title</th>
                <th>Content</th>
                <th>Quantity</th>
                <th>Type</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i=0;
            foreach($data as $d){
            	$i++;
                echo Form::open(["method"=>"POST","url"=>URL::action("ViewController@postSave")]);
                echo "<tr class='success'>";
                echo "<td><input type='text' name='keyword' value='".$d[0]."'></td>";
                echo "<td><input type='text' name='landpage' value='".$d[1]."'></td>";
                echo "<td><input type='text' name='title' value='".$d[2]."'></td>";
                echo "<td><textarea rows='5' cols='50' name='content' contenteditable='true' id='".$i."'>".$d[3]."</textarea></td>";
                echo "<td><input type='text' name='quantity' value='".$d[4]."'></td>";
                echo "<td><input type='text' name='type' value='".$d[5]."'></td>";
                echo "<input type='hidden' name='row' value='".$i."'>";
                echo "<input type='hidden' name='dpath' value='".$dpath."'>";
                echo "<input type='hidden' name='fname' value='".$fname."'>";
                echo "<td><input type='submit' class='btn btn-facebook' value='Submit'></td>";
                echo "</tr>";
                echo Form::close();
            }
            ?>
        </tbody>
    </table>
    <a class="btn btn-info" href="{{URL::action("ViewController@getDownFile",array('dpath'=>$dpath,'fname'=>$fname))}}"><span>{{trans("post.label.downfile")}}</span></a>
</div>
@stop