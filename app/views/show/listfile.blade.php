@section("content")
<div class='info'>
    <table class='table table-bordered'>
        <thead>
            <tr class='info'>
                <th>Tiêu đề</th>
                <th>Ngày lấy</th>
                <th>Tên Trang</th>
                <th>Loại</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach($result as $r){
                if ($r->type){
                    echo "<tr class='success'>";
                        echo "<td><a href='".URL::action("ViewController@getEditFile",["dpath"=>  explode("/", $r->link)[0],"fname"=>explode("/",$r->link)[1]])."'>".$r->title."</a></td>";
                        echo "<td>".$r->updated_at."</td>";
                        echo "<td>CNN</td>";
                        if ($r->isAnchor) echo "<td>Có Anchor Text</td>"; else echo "<td>Không Anchor Text</td>";
                    echo "</tr>";
                }else{
                    echo "<tr class='success'>";
                        echo "<td><a href='".URL::action("ViewController@getEditFile",["link"=>$r->link])."'>".$r->title."</a></td>";
                        echo "<td>".$r->updated_at."</td>";
                        echo "<td>VnExpress</td>";
                        if ($r->isAnchor) echo "<td>Có Anchor Text</td>"; else echo "<td>Không Anchor Text</td>";
                    echo "</tr>";
                }
            }
            ?>
        </tbody>
        <?php echo $result->links();?>
    </table>
</div>
@stop