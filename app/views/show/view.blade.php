@section("content")
<div class='info'>
    <table class='table table-bordered'>
        <thead>
            <tr class='info'>
                <th>Tiêu đề</th>
                <th>Ngày lấy</th>
                <th>Số Comment</th>
                <th>Status</th>
                <th>Xem bài viết</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $total = count($result["total"]);
            for($i = 0; $i<$total; $i++){
                if ($result["status"][$i]){
                    echo "<tr class='success'>";
                        echo "<td><a href='".URL::action("CommentListController@getThread",["t_id"=>$result["id"][$i]])."'>".$result["title"][$i]."</a><br><small>".$result["link"][$i]."</small></td>";
                        echo "<td>".$result["date"][$i]."</td>";
                        echo "<td>".$result["total"][$i]."</td>";
                        echo "<td>Đã lấy</td>";
                        echo "<td><a class='btn link' href='".$result["link"][$i]."'>Xem</a></td>";
                    echo "</tr>";
                }else{
                    echo "<tr class='warning'>";
                        echo "<td> No title<br><small>".$result["link"][$i]."</small></td>";
                        echo "<td>".$result["date"][$i]."</td>";
                        echo "<td>".$result["total"][$i]."</td>";
                        echo "<td>Chưa lấy</td>";
                        echo "<td><a class='btn link' href='".$result["link"][$i]."'>Xem</a></td>";
                    echo "</tr>";
                }
            }
            ?>
        </tbody>
    </table>
</div>
@stop
