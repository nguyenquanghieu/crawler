@section("content")
<div class='info'>
    <table class='table table-bordered'>
        <thead>
            <tr class='info'>
                <th>Tiêu đề</th>
                <th>Ngày lấy</th>
                <th>Số Comment</th>
                <th>Status</th>
                <th>Xem bài viết</th>
                <th>Đăng lên wordpress</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $total = count($result["total"]);
            for ($i = 0; $i < $total; $i++) {
                if ($result["status"][$i]) {
                    echo "<tr class='success'>";
                    echo "<td><a href='" . URL::action("CommentListController@getArticle", ["t_id" => $result["id"][$i]]) . "'>" . $result["title"][$i] . "</a><br><small>" . $result["link"][$i] . "</small></td>";
                    echo "<td>" . $result["date"][$i] . "</td>";
                    echo "<td>" . $result["total"][$i] . "</td>";
                    echo "<td>Đã lấy</td>";
                    echo "<td><a class='btn link' href='" . $result["link"][$i] . "'>Xem</a></td>";
                    $threadId = substr($result["link"][$i], -12);
                    $threadId = substr($threadId, 0, 7);
                    $checkStt = false;
                    for ($j = 0; $j < $result2['count']; $j++) {
                        if ($result2['thread_id'][$j] == $threadId && $result2['status'][$j] == true)
                            $checkStt = true;
                    }
                    if ($checkStt == false || $checkStt == NULL) {
                        echo "<td><a class='btn btn-default wordpressbtn' threadid='" . $threadId . "'>Đăng</a></td>";
                    } else {
                        echo "<td><a class='btn btn btn-warning wordpressrepostbtn' threadid='" . $threadId . "'>Đăng lại</a></td>";
                    }
                    echo "</tr>";
                } else {
                    echo "<tr class='warning'>";
                    echo "<td> No title<br><small>" . $result["link"][$i] . "</small></td>";
                    echo "<td>" . $result["date"][$i] . "</td>";
                    echo "<td>" . $result["total"][$i] . "</td>";
                    echo "<td>Chưa lấy</td>";
                    echo "<td><a class='btn link' href='" . $result["link"][$i] . "'>Xem</a></td>";
                    echo "<td><label class='btn btn-danger'" . $result["link"][$i] . "'>Chưa đăng được</label></td>";
                    echo "</tr>";
                }
            }
            ?>
        </tbody>
    </table>
</div>
@stop
@section("scripts")
<script type="text/javascript">
    $(document).ready(function() {
        $(".wordpressbtn").click(function(e) {
            console.log("clicked");
            var a = $(this).attr("threadid");
            console.log(a);
            $("#result").val("");
            var data = {'a': a};
            $.post('<?php echo URL::Action("SpamController@postToDatabase"); ?>',
                    data,
                    function(result) {
                        alert(result);
                    }
            );
        });
    });
    $(document).ready(function() {
        $(".wordpressrepostbtn").click(function(e) {
            console.log("clicked");
            var a = $(this).attr("threadid");
            console.log(a);
            $("#result").val("");
            var data = {'a': a};
            $.post('<?php echo URL::Action("SpamController@postRepostToDatabase"); ?>',
                    data,
                    function(result) {
                        alert(result);
                    }
            );
        });
    });
</script>
@stop
