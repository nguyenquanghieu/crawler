@section('content')

<?php
echo Form::open(array(
    'method' => 'post',
    'action' => 'SpamController@postTest',
    'files' => true,
    "class" => "form-horizontal",
    "role" => "form"
))
?>
  <div class="form-group">
    <label for="input-user-name" class="col-sm-2 control-label">User name</label>
    <div class="col-sm-10">
      <input name="name" type="email" class="form-control" id="input-user-name" placeholder="User name">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-10">
      <input name="pass" type="password" class="form-control" id="inputPassword3" placeholder="Password">
    </div>
  </div>
  <div class="form-group">
    <label for="input-link" class="col-sm-2 control-label">Link to wordpress page</label>
    <div class="col-sm-10">
      <input name="link" type="email" class="form-control" id="input-link" placeholder="Link to wordpress page">
    </div>
  </div>
  <div class="form-group">
    <label for="input-link" class="col-sm-2 control-label">Post title</label>
    <div class="col-sm-10">
      <input class="form-control" id="disabledInput" type="text" disabled value="aaa">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-danger">Post to wordpress</button>
    </div>
  </div>
<?php
echo Form::close();
?>
<a class="btn btn-info" id="test">Test</a>
<textarea id="result" class="col-sm-offset-2 col-sm-10" style="height: 200px;"></textarea>

@stop

@section('scripts')
<script type="text/javascript">

	$(document).ready(function(){
		$("#test").click(function(){
			console.log("clicked");
			$("#result").val("");
			var data;
			$.post('<?php echo URL::Action("SpamController@postPostComment"); ?>',
				data,
				function(result) {
					$("#result").val(result);
				}
			);
		});
	});

</script>

@stop