@section('content')
<?php
echo Form::open(array(
    'method' => 'post',
    'action' => 'CommentListController@postFile',
    'files' => true
))
?>
<fieldset>
    <div class="form-group">
        <label for=" ">Upload file:</label>
        <input type="file" id="upfile" name="upfile" value="Choose File"/>
    </div>
    <div class="form-group">
        <input type="hidden" id="a_id" name="a_id" value="{{$a_id}}"/>
    </div>
    <div class="control-group">
        <label class="control-label" for="Enter"></label>
        <div class="controls">
            <button type="submit" class="btn btn-info">Upload</button>
        </div>
    </div>
</fieldset>
<?php
echo Form::close();
?>
@stop