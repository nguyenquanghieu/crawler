@section('content')
<a class="btn btn-info" href="{{URL::action('CommentListController@getAnchorExport',array('c_id'=>$c_id))}}"><span>{{trans("post.label.anchortxt")}}</span></a>
<a class="btn btn-info" href="{{URL::action('CommentListController@getAnchorExcel',array('c_id'=>$c_id))}}"><span>{{trans("post.label.anchorxls")}}</span></a>
<br>
<a class="btn btn-link" href="/">Back</a>
@stop