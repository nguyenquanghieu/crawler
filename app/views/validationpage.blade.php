@section("content")
<?php
echo Form::open(array(
    'method' => 'get',
    'url' => URL::action('HtmlValidatorController@getResult')
))
?>
<fieldset class= "form-inline">
    <!-- Text input-->
    <div class="form-group">
        URL:<input type="text" class="form-control" name ="URL" placeholder="<?php echo $link; ?>">
    </div>

    <!-- Button -->
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <label class="control-label" for="Enter"></label>
            <button type="submit" class="btn btn-primary">Check!</button>
        </div>
    </div>

</fieldset>
<?php
echo Form::close()
?>
<?php
$html = file_get_html('validate.html');
//echo $link . '<br>';
$count = 0;
foreach ($html->find('img') as $element) {
    $element->src = 'http://validator.w3.org/' . $element->src;
}

foreach ($html->find('tr') as $element) {
    echo '<br><h4>' . $element . '<br>' . '</h4>';
    $count++;
    if ($count >= 1)
        break;
}
echo '<br>';
?>

<?php
$errArray = array();
foreach ($html->find("li") as $element) {
    array_push($errArray, $element);
}
$id = 1;
?>
<div class="panel-group" id="accordion">
    <?php
    $count = 1;
    for ($id; $id < count($errArray); $id++):
        if ($errArray[$id]->class == "msg_warn" || $errArray[$id]->class == "msg_err"):
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $id ?>">
                            <?php
                            if ($errArray[$id]->class == "msg_err") {
                                $printvar = "<strong>" . "Error: " . "</strong>";
                                $first = true;
                                foreach ($errArray[$id]->find('em') as $element1) {
                                    if (!is_null($element1))
                                        $printvar = "<strong>" . "Error: " . $element1 . "</strong>";
                                    $first = false;
                                    if ($first == false)
                                        break;
                                }
                                echo $printvar;
                            };
                            if ($errArray[$id]->class == "msg_warn") {
                                $printvar = "<strong>" . "Error: " . "</strong>";
                                $first = true;
                                foreach ($errArray[$id]->find('em') as $element1) {
                                    if (!is_null($element1))
                                        $printvar = "<strong>" . "Warning: " . $element1 . "</strong>";
                                    $first = false;
                                    if ($first == false)
                                        break;
                                }
                                echo $printvar;
                            };
                            ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse-<?php echo $id ?>" class="panel-collapse collapse<?php if ($count == 1) {
                                echo ' in';
                            } else {
                                echo '';
                            } ?>">
                    <div class="panel-body">
        <?php echo $errArray[$id];
        $count++; ?>
                    </div>
                </div>
            </div>
        <?php
    endif;
endfor;
?>
    @stop


